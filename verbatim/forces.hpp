#ifndef VERBATIM_FORCES_HPP
#define VERBATIM_FORCES_HPP

#include <string>
#include <sstream>
#include <fstream>
#include "config.hpp"
#include "precondition.hpp"

template <typename real>
class forces {
    public:
        typedef typename vex::cl_vector_of<real, 2>::type real2;

        VEX_FUNCTION_SD(
                real2,
                local_force,
                (real2, pos)(real2, vel)(int, type)(real, mass)(real, time),
                (bforce),
                local_force_body()
                );

        VEX_FUNCTION_S(
                real2,
                potential_interaction,
                (real2, p1)(real2, p2)(int, type1)(int, type2),
                potential_interaction_body()
                );

    private:
        VEX_FUNCTION(real2, lj, (real2, x)(real2, y)(real, sigma)(real, eps),
                real2 d = x - y;
                real  r = length(d);
                if (r < 1e-8) return 0;

                real c  = sigma / r;
                real c3 = c * c * c;
                real c6 = c3 * c3;

                return -d * eps * c6 * (24 - 48 * c6) / (r * r);
            );

        VEX_FUNCTION_S(real2, bforce, (real, time)(real2, pos),
                config::bforce
            );

        static std::string local_force_body() {
            std::ostringstream src;

            double mx = 0.5 * (domain::lo.s[0] + domain::hi.s[0]);
            src << std::scientific << std::setprecision(8) <<
                "if (type == 0) return bforce(time, pos);\n"
                "real2 a = " << -config::gamma << " * vel "
                " - mass * (real2)(0, " << config::grav << ");\n"
                "if (fabs(time - " << config::tunlock << ") < 1e-2 "
                "&& length(pos - (real2)(" << mx <<
                ", 0)) < 5) a += (real2)(1e6 * sign(pos.x - " << mx << "), 0);\n"
                "return a;";

            return src.str();
        }

        static std::string potential_interaction_body() {
            // Read potentials.
            std::string error_msg = "Error reading the potential file ("
                + config::potential_file + ")";

            std::ifstream f(config::potential_file);
            precondition(f, error_msg);

            unsigned ntypes;
            precondition(f >> ntypes, error_msg);
            f.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            std::ostringstream src;
            src << std::scientific << std::setprecision(8);

            src << "switch (type1) {\n";

            std::string pbody;
            for(unsigned i = 0; i < ntypes; ++i) {
                src <<
                    "case " << i << ":\n"
                    "  switch (type2) {\n";
                for(unsigned j = 0; j < ntypes; ++j) {
                    std::getline(f, pbody);
                    src <<
                        "  case " << j << ":\n"
                        "  {\n" << pbody <<
                        "  }\n";
                }
                src << "  }\n";
            }
            src << "}\n";

            return src.str();
        }
};

#endif

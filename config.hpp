#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <string>

namespace config {

extern std::string coor_file;
extern std::string conn_file;
extern std::string potential_file;
extern std::string save_file;
extern std::string init_file;
extern std::string bforce;

extern float    gamma;
extern float    grav;
extern float    k;
extern bool     freeze;
extern float    cell_size;
extern float    tau;
extern float    tunlock;
extern float    tmax;
extern unsigned wstep;
extern int      lock_type;

void read(int argc, char *argv[]);

}

#endif

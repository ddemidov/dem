#!/usr/bin/python
import numpy as np

hm = 1.0
hf = 0.5
hw = 0.2

Ht = 400 # Tank height
Lt = 200 # Tank length

Hr = 50  # Reservoir height
Hn = 50  # Neck height
Hs = 800 # Shaft height
Ws = 10  # Shaft width

Y0 = Ws * 3
Y1 = Y0 + Hs
Y2 = Y1 + Hn
Y3 = Y2 + Hr

X0 = Lt / 2 - Ws / 2 - Hn
X1 = X0 + Hn
X2 = X1 + Ws
X3 = X2 + Hn

part = []
conn = []

m_wall = 1
m_int  = 1.5
m_ext  = 1

t_wall = 0
t_int  = 1
t_ext  = 2
t_lock = 3

#---------------------------------------------------------------------------
# Tank walls
#---------------------------------------------------------------------------
x = X1
y = Ht

part.append( (x, y, m_wall, t_wall) )

while x >= 0:
    x = x - hw
    part.append( (x, y, m_wall, t_wall) )

while y >= 0:
    y = y - hw
    part.append( (x, y, m_wall, t_wall) )

while x <= Lt:
    x = x + hw
    part.append( (x, y, m_wall, t_wall) )

while y <= Ht:
    y = y + hw
    part.append( (x, y, m_wall, t_wall) )

while x >= X2:
    x = x - hw
    part.append( (x, y, m_wall, t_wall) )

#---------------------------------------------------------------------------
# Shaft
#---------------------------------------------------------------------------
x = Lt / 2 - Ws / 2 - Hn
y = Y3

part.append( (x, y, m_wall, t_wall) )

while y >= Y2:
    y = y - hw
    part.append( (x, y, m_wall, t_wall) )

while y >= Y1:
    x = x + hw
    y = y - hw
    part.append( (x, y, m_wall, t_wall) )

while y >= Y0 + 5 * hm:
    y = y - hw
    part.append( (x, y, m_wall, t_wall) )

while y >= Y0:
    y = y - hw
    part.append( (x, y, m_wall, t_lock) )

while x <= X2:
    x = x + hw
    part.append( (x, y, m_wall, t_wall) )

while y <= Y0 + 5 * hm:
    y = y + hw
    part.append( (x, y, m_wall, t_lock) )

while y <= Y1:
    y = y + hw
    part.append( (x, y, m_wall, t_wall) )

while y <= Y2:
    x = x + hw
    y = y + hw
    part.append( (x, y, m_wall, t_wall) )

while y <= Y3:
    y = y + hw
    part.append( (x, y, m_wall, t_wall) )

while x >= X0:
    x = x - hw
    part.append( (x, y, m_wall, t_wall) )

#---------------------------------------------------------------------------
# Medium
#---------------------------------------------------------------------------
nx = int(Lt / hm) - 2;
ny = int(Ht / hm) - 2;

x0 = hm
y0 = hm

for j in xrange(0, ny):
    for i in xrange(0, nx):
        x = x0 + i * hm
        y = y0 + j * hm

        if x >= X1 - hm and x <= X2 + hm and y >= Y0 - hm: continue

        part.append( (x, y, m_int, t_int) );

#---------------------------------------------------------------------------
# Frakking liquid
#---------------------------------------------------------------------------

nx = int((X3 - X0) / hf) - 2
ny = int((Y3 - Y0) / hf) - 2

for j in xrange(0, ny):
    for i in xrange(0, nx):
        x = X0 + hf + i * hf
        y = Y0 + hf + j * hf

        if y < Y2:
            if x < X1 + hf or x > X2 - hf: continue
        elif y < Y3:
            if x < X0 + hf or x > X3 - hf: continue


        part.append( (x, y, m_ext, t_ext) )

#---------------------------------------------------------------------------
# Save the files
#---------------------------------------------------------------------------
pf = open('part.txt', 'w')
pf.write("%s\n" % len(part))

for p in part:
    pf.write("%s %s 0 0 %s %s\n" % p)

cf = open('conn.txt', 'w')
cf.write("%s\n" % len(conn))

for c in conn:
    cf.write("%s %s\n" % c)

open("dem.cfg", "w").write(
"""
tau = 1e-4
wstep = 1000
tunlock = 20
lock = 3
"""
)

#---------------------------------------------------------------------------
# Potentials
#---------------------------------------------------------------------------
class zero:
    def write(self, f):
        f.write("return 0;\n")

class lj:
    def __init__(self, sigma, eps):
        self.sigma = sigma
        self.eps   = eps

    def write(self, f):
        f.write(
            "real2 d = p1 - p2; "
            "real  r = length(d); "
            "if (r < 1e-8) return 0; "
            "real c  = %s / r; "
            "real c3 = c * c * c; "
            "real c6 = c3 * c3; "
            "return d * %s * c6 * (48 * c6 - 24) / (r * r);\n" % (
                self.sigma, self.eps)
            )

class zig:
    def __init__(self, sigma, eps):
        self.sigma = sigma
        self.eps   = eps

    def write(self, f):
        f.write(
            "real2 d = p1 - p2; "
            "real  r = length(d); "
            "if (r < 1e-8) return 0; "
            "real c  = 1.28 * %s / r; "
            "real c2 = c * c; "
            "real c3 = c2 * c; "
            "real c6 = c3 * c3; "
            "return d * %s * c6 * (12 * c6 - 36 * c3 + 14.4 * c2 + 6) / (r * r);\n" % (
                self.sigma, self.eps)
            )

potentials = [
        [zero(),       zero(),        zero()      ],
        [lj(1.0, 0.1), zig(1.0, 1e3), zig(1.0, 1e3)],
        [lj(1.0, 0.1), zig(1.0, 1e3), lj (0.5, 0.1)]
        ]

n = len(potentials)
f = open('potential.txt', 'w')
f.write("%s\n" % n)

for row in range(0, n):
    for col in range(0, n):
        potentials[row][col].write(f)

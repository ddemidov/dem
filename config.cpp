#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <boost/program_options.hpp>
#include "config.hpp"

namespace config {

std::string coor_file      = "part.txt";
std::string conn_file      = "conn.txt";
std::string potential_file = "potential.txt";
std::string save_file      = "dem";
std::string init_file      = "";
std::string bforce         = "return 0;";

float    gamma     = 0.1f;
float    grav      = 9.8f;
float    k         = 1e6f;
float    cell_size = 2.0f;
float    tau       = 0.0005f;
float    tunlock   = std::numeric_limits<float>::max();
float    tmax      = 40.0f;
unsigned wstep     = 100;
bool     freeze    = true;
int      lock_type = -1;

template <typename T>
std::string to_string(const T &val) {
    std::ostringstream s;
    s << std::setprecision(3) << val;
    return s.str();
}

void read(int argc, char *argv[]) {
    namespace po = boost::program_options;
    po::options_description desc("Options");

    std::string conf_file = "dem.cfg";

#define OPTION(option, name, descr)                                            \
    (option,                                                                   \
     po::value<decltype(name)>(&name)->default_value(name, to_string(name)),   \
     descr                                                                     \
    )

    desc.add_options()
        ("help,h", "Show help")
        OPTION("cfg", conf_file, "Configuration file")
        OPTION("save,o", save_file, "Output file")
        OPTION("initial,0", init_file, "Initial conditions file")
        OPTION("part,p", coor_file, "Particles file")
        OPTION("conn,c", conn_file, "Connections file")
        OPTION("pot,i", potential_file, "Potentials file")
        OPTION("gamma", gamma, "Viscous friction coefficient")
        OPTION("grav", grav, "Gravity acceleration")
        OPTION("k", k, "Hooke's law coefficient")
        OPTION("cell_size", cell_size, "Bucket size (used for interaction optimization)")
        OPTION("tau", tau, "Time step")
        OPTION("tunlock", tunlock, "Start pumping at this time")
        OPTION("tmax", tmax, "Maximum time")
        OPTION("wstep", wstep, "Write solution every wstep time steps")
        OPTION("freeze", freeze, "Freeze particles that have flown too far")
        OPTION("bforce", bforce,
         "Source string for boundary force function body. "
         "The string may use variables \"time\" and \"pos\". "
         "Beware though that position-dependent force may "
         "break boundary integrity.\n"
         "Example: \"return (real2)(sin(time), 0);\""
        )
        OPTION("lock", lock_type, "Particle type of the lock")
        ;

#undef OPTION
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        exit(0);
    }

    std::ifstream cfg(conf_file);
    if (cfg) {
        po::store(po::parse_config_file(cfg, desc), vm);
        po::notify(vm);
    }
}

}

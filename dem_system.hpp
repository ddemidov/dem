#ifndef DEM_SYSTEM_HPP
#define DEM_SYSTEM_HPP

#include <vector>
#include <array>
#include <tuple>
#include <fstream>
#include <iterator>

#include <vexcl/vexcl.hpp>

#include "precondition.hpp"
#include "config.hpp"

template <typename real, class forces>
class dem_system {
    public:
        typedef typename vex::cl_vector_of<real, 2>::type real2;

        typedef real2   point_type;
        typedef cl_int2 index_type;
        typedef cl_uint hash_type;

        dem_system(
                const vex::Context                           &ctx,
                const std::vector< point_type              > &points,
                const std::vector< real                    > &mass,
                const std::vector< int                     > &type,
                const std::vector< std::array<unsigned, 2> > &conn
              ) : ctx(ctx), n(points.size()), min(ctx), max(ctx),
                  mass(ctx, mass), type(ctx, type), cell_coo(ctx, n),
                  cell_idx(ctx, n), part_ord(ctx, n)
        {
            precondition(mass.size() == n && type.size() == n, "Size mismatch");

            // Prepare body structure.
            auto conn_data = reinterpret_cast<const unsigned*>(conn.data());
            std::vector<unsigned> h_id(conn_data, conn_data + 2 * conn.size());

            std::sort(h_id.begin(), h_id.end());
            h_id.erase(std::unique(h_id.begin(), h_id.end()), h_id.end());

            size_t nbody = h_id.size();

            std::vector<unsigned> b_id(n);
            std::vector<unsigned> h_ptr(nbody + 1, 0);

            for(unsigned i = 0; i < h_id.size(); ++i)
                b_id[h_id[i]] = i;

            for(auto c = conn.begin(); c != conn.end(); ++c) {
                ++h_ptr[1 + b_id[(*c)[0]]];
                ++h_ptr[1 + b_id[(*c)[1]]];
            }

            std::partial_sum(h_ptr.begin(), h_ptr.end(), h_ptr.begin());

            body.idx.resize(ctx, h_id);
            body.ptr.resize(ctx, h_ptr);

            std::vector<unsigned> h_conn(h_ptr.back());
            std::vector<real>     h_dist(h_ptr.back());

            for(auto c = conn.begin(); c != conn.end(); ++c) {
                unsigned i  = (*c)[0];
                unsigned j  = (*c)[1];
                unsigned ii = b_id[i];
                unsigned jj = b_id[j];

                real d = distance(points[i], points[j]);

                h_conn[h_ptr[ii]] = j;
                h_dist[h_ptr[ii]] = d;

                h_conn[h_ptr[jj]] = i;
                h_dist[h_ptr[jj]] = d;

                ++h_ptr[ii];
                ++h_ptr[jj];
            }

            body.conn.resize(ctx, h_conn);
            body.dist.resize(ctx, h_dist);

            // Find bounding box.
            bbox.lo = bbox.hi = points.front();
            for(auto p = points.begin(); p != points.end(); ++p) {
                bbox.lo.s[0] = std::min(bbox.lo.s[0], p->s[0]);
                bbox.lo.s[1] = std::min(bbox.lo.s[1], p->s[1]);

                bbox.hi.s[0] = std::max(bbox.hi.s[0], p->s[0]);
                bbox.hi.s[1] = std::max(bbox.hi.s[1], p->s[1]);
            }

            bbox.lo.s[0] -= 2 * config::cell_size;
            bbox.lo.s[1] -= 2 * config::cell_size;

            bbox.hi.s[0] += 2 * config::cell_size;
            bbox.hi.s[1] += 2 * config::cell_size;
        }

        void operator()(
                vex::vector<point_type> const &p,
                vex::vector<point_type> const &v,
                vex::vector<point_type>       &a,
                real time
                ) const
        {
            using namespace vex;

            VEX_FUNCTION(hash_type, get_cell_hash, (index_type, c)(cl_uint, nx),
                    return c.y * nx + c.x;
                );

            VEX_FUNCTION(hash_type, hi_bound, (const hash_type*, x)(size_t, n)(size_t, v),
                    size_t begin = 0;
                    size_t end   = n;
                    while(end > begin) {
                        size_t mid = begin + (end - begin) / 2;
                        if (x[mid] <= v)
                            begin = ++mid;
                        else
                            end = mid;
                    }
                    return begin;
                );

            VEX_FUNCTION_D(real2, interaction,
                    (cl_uint,        nx)
                    (cl_uint,        nc)
                    (cl_uint,        nt)
                    (real,           time)
                    (cl_uint,        cur)
                    (const cl_int2*, cell_coo)
                    (const cl_uint*, part_ord)
                    (const cl_uint*, cell_ptr)
                    (const real2*,   P)
                    (const real2*,   V)
                    (const real*,    M)
                    (const cl_int*,  T),
                    (get_cell_hash)
                    (forces::local_force)
                    (forces::potential_interaction),

                    int   t    = T[cur];
                    real2 p    = P[cur];
                    int2 index = cell_coo[cur];

                    real2 a  = local_force(p, V[cur], t, M[cur], time);

                    for(int i = -1; i <= 1; ++i) {
                        for(int j = -1; j <= 1; ++j) {
                            int2 cell_index = index + (int2)(i, j);
                            uint cell_hash  = get_cell_hash(cell_index, nx);
                            if (cell_hash >= nc) continue;

                            for(uint ii = cell_ptr[cell_hash], ee = cell_ptr[cell_hash + 1]; ii < ee; ++ii) {
                                uint jj = part_ord[ii];
                                if (jj == cur) continue;

                                a += potential_interaction(p, P[jj], t, T[jj]);
                            }
                        }
                    }

                    return a;
                );

            VEX_FUNCTION(real2, hookes_law,
                    (unsigned,        i)
                    (unsigned,        idx)
                    (real,            K)
                    (const int*,      type)
                    (const unsigned*, ptr)
                    (const unsigned*, conn)
                    (const real*,     dist)
                    (const real2*,    P),

                    if (type[idx] == 0) return 0;

                    real2 F = {0, 0};
                    real2 x = P[idx];

                    for(unsigned j = ptr[i], e = ptr[i + 1]; j < e; ++j) {
                        real2 y  = P[conn[j]];
                        real  d0 = dist[j];
                        real2 r  = y - x;
                        real  d  = length(r);

                        F += K * (r * (d - d0) / d);
                    }

                    return F;
                );

            // Compute bounding domain consisting of cells.
            point_type p_min, p_max;
            std::tie(p_min, p_max) = get_domain(p);

            unsigned n_cell_x = (p_max.s[0] - p_min.s[0]) / config::cell_size;
            unsigned n_cell_y = (p_max.s[1] - p_min.s[1]) / config::cell_size;
            unsigned n_cells  = n_cell_x * n_cell_y;

            if (cell_ptr.size() != n_cells + 1) {
                cell_ptr.resize(ctx, n_cells + 1);
                cell_ptr[0] = 0;
            }

            // Assign each particle to a cell, reset ordering.
            cell_coo = convert_int2( (p - p_min) / config::cell_size );
            cell_idx = get_cell_hash(cell_coo, n_cell_x);
            part_ord = element_index();

            // Sort particle numbers in part_ord by cell idx.
            sort_by_key(cell_idx, part_ord);

            // Find range of each cell in sorted cell_idx array.
            permutation(element_index(1, n_cells + 1))(cell_ptr) = hi_bound(
                    raw_pointer(cell_idx), n, element_index(0, n_cells));

            // Compute acceleration.
            a = interaction(
                    n_cell_x, n_cells, ntypes, time,
                    element_index(),
                    raw_pointer(cell_coo),
                    raw_pointer(part_ord),
                    raw_pointer(cell_ptr),
                    raw_pointer(p),
                    raw_pointer(v),
                    raw_pointer(mass),
                    raw_pointer(type)
                    );

            if (body.conn.size())
                permutation(body.idx)(a) += hookes_law(
                        element_index(), body.idx, config::k,
                        raw_pointer(type),
                        raw_pointer(body.ptr),
                        raw_pointer(body.conn),
                        raw_pointer(body.dist),
                        raw_pointer(p)
                        );

            a /= mass;
        }

        void freeze_outcasts(
                vex::vector<point_type> &p,
                vex::vector<point_type> &v
                )
        {
            VEX_FUNCTION(bool, outcast, (real2, p)(real2, lo)(real2, hi),
                    if (p.x < lo.x) return true;
                    if (p.y < lo.y) return true;
                    if (p.x > hi.x) return true;
                    if (p.y > hi.y) return true;
                    return false;
                    );

            auto to_fix = vex::make_temp<1>( outcast(p, bbox.lo, bbox.hi) );

            vex::tie(p, v, type) = std::tie(
                    if_else(to_fix, bbox.lo, p),
                    if_else(to_fix, 0, v),
                    if_else(to_fix, 0, type)
                    );
        }

        void freeze_surface(
                vex::vector<point_type> const &p,
                vex::vector<point_type>       &v
                )
        {
            const point_type zero = {{0, 0}};

            VEX_FUNCTION(bool, surf, (real2, p)(real, h1)(real, h2)(cl_int, type),
                    if (p.y >= h1 && p.y <= h2 && type == 1) return true;
                    return false;
                    );

            auto to_fix = vex::make_temp<1>( surf(p, 200, 201, type) );

            vex::tie(v, type) = std::tie(
                    if_else(to_fix, zero, v   ),
                    if_else(to_fix, 0,    type)
                    );
        }
    private:
        const vex::Context &ctx;

        unsigned n, ntypes;

        vex::Reductor<real, vex::MIN> min;
        vex::Reductor<real, vex::MAX> max;

        vex::vector<real> mass;
        vex::vector<int>  type;
        vex::vector<real> sigma;
        vex::vector<real> eps;

        mutable vex::vector<index_type> cell_coo;
        mutable vex::vector<hash_type>  cell_idx;
        mutable vex::vector<hash_type>  part_ord;
        mutable vex::vector<hash_type>  cell_ptr;

        struct {
            vex::vector<unsigned> idx;
            vex::vector<unsigned> ptr;
            vex::vector<unsigned> conn;
            vex::vector<real>     dist;
        } body;

        struct {
            point_type hi;
            point_type lo;
        } bbox;

        std::tuple<point_type, point_type> get_domain(
                vex::vector<point_type> const &p) const
        {
            VEX_FUNCTION(real, X, (real2, p), return p.x; );
            VEX_FUNCTION(real, Y, (real2, p), return p.y; );

            point_type p_min = {{ min(X(p)), min(Y(p)) }};
            point_type p_max = {{ max(X(p)), max(Y(p)) }};

            real spanx = p_max.s[0] - p_min.s[0];
            real spany = p_max.s[1] - p_min.s[1];

            real dx = config::cell_size - fmod(spanx, config::cell_size);
            real dy = config::cell_size - fmod(spany, config::cell_size);

            p_min.s[0] -= dx / 2 + config::cell_size;
            p_min.s[1] -= dy / 2 + config::cell_size;
            p_max.s[0] += dx / 2 + config::cell_size;
            p_max.s[1] += dy / 2 + config::cell_size;

            return std::make_tuple(p_min, p_max);
        }

        static real distance(point_type p1, point_type p2) {
            real dx = p1.s[0] - p2.s[0];
            real dy = p1.s[1] - p2.s[1];
            return sqrt(dx * dx + dy * dy);
        }
};

#endif

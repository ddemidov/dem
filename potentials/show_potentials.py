#!/usr/bin/python
import sys

if (len(sys.argv[1:]) > 0):
    import matplotlib
    matplotlib.use('Agg')
    outfile = sys.argv[1]
    quiet = True
else:
    quiet = False

from pylab import *

#----------------------------------------------------------------------------
def lennard_jones(r, sigma, eps):
    c = sigma / r;
    return 4 * eps * (c**12 - c**6)

def zigzag1(r, sigma, eps):
    c = 1.28 * sigma / r
    return eps * ( c**12 - 4 * c**9 + 1.8 * c**8 + c**6 );

def zigzag2(r, sigma, eps):
    c = 1.45 * sigma / r
    return 0.15 * eps * ( c**12 - 4 * c**9 + 1.5 * c**8 );

#----------------------------------------------------------------------------
figure(num=1, figsize=(7,7))
r = np.logspace(-3, 1, 1000)
plot(r, 0 * r, 'k:')
plot(r, lennard_jones(r, 1, 1), 'k-', label='Lennard Jones')
plot(r, zigzag1(r, 1, 1),       'r-', label='Zigzag1')
plot(r, zigzag2(r, 1, 1),       'b-', label='Zigzag2')

xlabel('r')
ylabel('V')
legend(loc='upper right', frameon=False)
xlim([ 0, 4])
xticks(range(0, 5))
ylim([-2, 4])

if quiet: savefig(outfile)
else:     show()

